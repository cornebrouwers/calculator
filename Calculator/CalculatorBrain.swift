//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Corne Brouwers on 4/24/16.
//  Copyright © 2016 Corne Brouwers. All rights reserved.
//

import Foundation

class CalculatorBrain {
    
    var BinaryCalculation = false
    
    struct PartialCalculation {
        var op1 : Double
        var operation : String
    }
    
    var calc: PartialCalculation = PartialCalculation.init(op1: 0.0, operation: "")
    var result = 0.0
    
    private var accumulator = 0.0
    
    func addKeyStrokeToAccumulator(key: String) {
        accumulator = Double(key)!
    }

    func addBinaryOperator(key: String) {
        
        if BinaryCalculation {
            switch calc.operation {
            case "×" : result = calc.op1 * accumulator
            case "÷" : result = calc.op1 / accumulator
            case "+" : result = calc.op1 + accumulator
            case "−" : result = calc.op1 - accumulator
            default: break
            }
            if key == "=" {
                accumulator = result
                BinaryCalculation = false
            } else {
                calc.op1 = result
                calc.operation = key
            }
        } else {
            BinaryCalculation = true
            calc.operation = key
            calc.op1 = accumulator
        }
    }

    func addUnaryOperator(key: String) {
        switch key {
        case "sin": result = sin(accumulator)
        case "cos": result = cos(accumulator)
        case "π":   result = M_PI
        case "√":   result = sqrt(accumulator)
        default: break
        }
        accumulator = result
        BinaryCalculation = false
    }
}