//
//  ViewController.swift
//  Calculator
//
//  Created by Corne Brouwers on 4/24/16.
//  Copyright © 2016 Corne Brouwers. All rights reserved.
//

import UIKit

class CalcViewController: UIViewController {
    @IBOutlet weak var display: UILabel!
    private var calculator = CalculatorBrain()
    private var startNewStroke = true {
        didSet { if startNewStroke { decimalPressedAlready = false }}
    }
    private var decimalPressedAlready = false
    private var keyStroke = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func processKey(key: String) -> Void {
        if startNewStroke {
            if key == "." {
                keyStroke = "0."
            } else {
                keyStroke = key
            }
            startNewStroke = false
        } else {
            keyStroke = keyStroke + key
        }
        display.text = keyStroke
    }
    

    @IBAction func numberPressed(sender: UIButton) {
        processKey(sender.currentTitle!)
    }
    
    @IBAction func decimalPressed(sender: UIButton) {
        if !decimalPressedAlready {
            decimalPressedAlready = true
            processKey(sender.currentTitle!) // ToDo: make it 0. if it's the first key to have been pressed
        }
    }
    
    @IBAction func BinaryOperationPressed(sender: UIButton) {
        calculator.addKeyStrokeToAccumulator(display.text!)
        startNewStroke = true
        keyStroke = sender.currentTitle!
        calculator.addBinaryOperator(keyStroke)
        display.text = String(calculator.result)
    }

    @IBAction func UnaryOperationPressed(sender: UIButton) {
        calculator.addKeyStrokeToAccumulator(display.text!)
        startNewStroke = true
        keyStroke = sender.currentTitle!
        calculator.addUnaryOperator(keyStroke)
        display.text = String(calculator.result)
    }
}

